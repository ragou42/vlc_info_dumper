
--------------------------

function descriptor()
	return {
		title = "track_title_writeout",
		version = "0.0",
		author = "ragou42",
		url = "https://gitlab.com/ragou42/vlc_info_dumper",
		shortdesc = "save current title and artist to file.",
		description = "save current title and artist to file.",
		capabilities = { "input-listener"; "meta-listener"; "playing-listener" }
	}
end

function activate()
	trackfile = "/tmp/track.txt"
	artistfile = "/tmp/artist.txt"
	combinedfile = "/tmp/artistAndTrack.txt"
	vlc.msg.info("Setting output dir" .. vlc.config.userdatadir())
	--vlc.misc.userdatadir()..
	--vlc.var.add_callback( vlc.object.libvlc(), "key-pressed", key_press )
	return false
end

function deactivate()
	--vlc.var.del_callback( vlc.object.libvlc(), "key-pressed", key_press )
	return true
end
function close()
    vlc.deactivate()
end

function playing_changed()
	return input_changed()
	
end

function meta_changed()
	return true
end

function input_changed()
	collectgarbage()
	vlc.msg.info("Input has changed!") 
	local input = vlc.playlist
	local item = vlc.input.item() --vlc.var.get(input, ".name") .. "testing"
	local track = "  *  "..item:name().."  *  "
	io.output(trackfile)
	io.write(track)
	io.close()
	local artist = "  *  "..item:metas()["artist"].."  *  "
	io.output(artistfile)
	io.write(artist)
	io.close()
	local artistAndTrack = "  *  "..item:metas()["artist"].." : "..item:name().."  *  "
	io.output(combinedfile)
	io.write(artistAndTrack)
	io.close()
	return true
end